
#ifndef __inc_cnce_router_config_hpp__
#define __inc_cnce_router_config_hpp__

#include <inttypes.h>

namespace cnce
{

const uint32_t router_num_steppers ( 6 );
const uint32_t router_num_sensors ( 10 );
const uint32_t router_num_switches ( 10 );
const uint32_t router_num_outputs ( 22 ); // num_switches + num_steppers * 2
const uint32_t router_num_frequency_generators ( 3 );
const uint32_t router_num_stepper_sync_groups ( router_num_steppers );

const uint32_t router_frequency_generator_usecs_min ( 10 );
const uint16_t router_pulse_usecs_min ( 10 );
const uint16_t router_stepper_queue_capacity ( 1024 );

} // namespace cnce

#endif
