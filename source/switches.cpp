/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "switches.hpp"
#include <adue/timer_counter.hpp>
#include <cnce/router/config.hpp>

// Variables extern

uint32_t switches_states;
uint32_t switches_states_sent;
::cnce::Router_Switch switches[::cnce::router_num_switches ];

// Function declarations

// Function definitions

void
setup_switches ()
{
  switches_states = 0;
  switches_states_sent = 0;
}

void
switches_reset ()
{
  switches_set_all_low ();
  switches_states_sent = 0;
}

void
switches_set_all_low ()
{
  switches_states = 0;
  for ( unsigned int ii = 0; ii != ::cnce::router_num_switches; ++ii ) {
    switches[ ii ].set_low ();
  }
}

void
set_switch_low ( uint32_t switch_index_n )
{
  if ( switch_index_n < ::cnce::router_num_switches ) {
    const uint32_t mask32 ( 1 << switch_index_n );
    if ( ( switches_states & mask32 ) != 0 ) {
      switches_states &= ~mask32;
      switches[ switch_index_n ].set_low ();
    }
  }
}

void
set_switch_high ( uint32_t switch_index_n )
{
  if ( switch_index_n < ::cnce::router_num_switches ) {
    const uint32_t mask32 ( 1 << switch_index_n );
    if ( ( switches_states & mask32 ) == 0 ) {
      switches_states |= mask32;
      switches[ switch_index_n ].set_high ();
    }
  }
}
