/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_frequency_generators_hpp__
#define __inc_router_frequency_generators_hpp__

#include "frequency_generator.hpp"
#include <cnce/router/config.hpp>

// Variables extern

extern ::cnce::Router_Frequency_Generator
    frequency_generators[::cnce::router_num_frequency_generators ];

// Function declarations

extern void
setup_frequency_generators ();

#endif
