/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_router_stepper_job_expanded_hpp__
#define __inc_cnce_router_stepper_job_expanded_hpp__

#include "stepper_job.hpp"
#include <inttypes.h>

namespace cnce
{

class Router_Axis_Job_Expanded
{
  // Public methods
  public:
  uint8_t
  job_type () const;

  void
  set_job_type ( uint8_t num_n );

  bool
  is_movement_job () const;

  bool
  is_switch_job () const;

  uint32_t
  ticks () const;

  void
  set_ticks ( uint32_t num_n );

  uint32_t
  switch_index () const;

  void
  set_switch_index ( uint32_t index_n );

  // Private attributes
  private:
  uint8_t _job_type;
  uint32_t _ticks;
};

inline uint8_t
Router_Axis_Job_Expanded::job_type () const
{
  return _job_type;
}

inline void
Router_Axis_Job_Expanded::set_job_type ( uint8_t type_n )
{
  _job_type = type_n;
}

inline bool
Router_Axis_Job_Expanded::is_movement_job () const
{
  return ::cnce::router_stepper_job_type_is_movement ( _job_type );
}

inline bool
Router_Axis_Job_Expanded::is_switch_job () const
{
  return ::cnce::router_stepper_job_type_is_switch ( _job_type );
}

inline uint32_t
Router_Axis_Job_Expanded::ticks () const
{
  return _ticks;
}

inline void
Router_Axis_Job_Expanded::set_ticks ( uint32_t num_n )
{
  _ticks = num_n;
}

inline uint32_t
Router_Axis_Job_Expanded::switch_index () const
{
  return _ticks;
}

inline void
Router_Axis_Job_Expanded::set_switch_index ( uint32_t num_n )
{
  _ticks = num_n;
}

} // namespace cnce

#endif
