/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "steppers.hpp"
#include "communication.hpp"
#include "switches.hpp"
#include <adue/pmc.hpp>
#include <adue/tc/channel.hpp>

// Variables extern

::cnce::Router_Stepper steppers[::cnce::router_num_steppers ];
adue::Ring_Buffer< ::cnce::Stepper_Sync_Group *,
                   ::cnce::router_num_stepper_sync_groups >
    stepper_sync_groups_ready;
::cnce::Stepper_Sync_Group
    stepper_sync_groups[::cnce::router_num_stepper_sync_groups ];

// Variables static

volatile uint32_t stepper_queue_changed[::cnce::router_num_steppers ];
static uint32_t pulse_ticks_high_default;
static const unsigned int stepper_timer_offset = 3;

// Function declarations

void
steppers_feed ();

void
steppers_start_synced ( ::cnce::Stepper_Sync_Group & sync_group_n );

template < uint8_t STEPPER_INDEX >
void
stepper_start_synced_load_pulses ();

template < uint8_t STEPPER_INDEX >
void
stepper_start_synced_start_timers ();

template < uint8_t STEPPER_INDEX >
void
stepper_clear_job_queues ();

template < uint8_t STEPPER_INDEX >
void
stepper_feed ();

template < uint8_t STEPPER_INDEX, bool USE_TIMER_OFFSET >
void
stepper_load_next_pulse ();

template < uint8_t STEPPER_INDEX >
void
stepper_timer_interrupt ();

// Function definitions

template < uint8_t STEPPER_INDEX >
void
stepper_timer_setup ()
{
  typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;
  adue::pmc_set_writeprotect ( false );
  adue::pmc_enable_periph_clk ( FTimer::peripheral_id () );

  {
    // Disable TC clock / Stop
    FTimer::clock_disable ();
    // Disable interrupts
    FTimer::interrupt_disable ( 0xFFFFFFFF );
    // Clear status register
    FTimer::status ();
    FTimer::ra_set ( 0xFFFFFFFF );
    FTimer::rb_set ( 0xFFFFFFFF );
    FTimer::rc_set ( 0xFFFFFFFF );
    // Set mode
    FTimer::set_mode ( TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC |
                       TC_CMR_TCCLKS_TIMER_CLOCK1 );
    // Enable interrupts
    FTimer::interrupt_enable_ra ();
    FTimer::interrupt_enable_rc ();
  }

  // Enable interrupt controller interrupts
  NVIC_EnableIRQ ( FTimer::irqn () );
}

inline constexpr uint32_t
stepper_timer_ticks_per_us ()
{
  // The selected TIMER_CLOCK1 is the main clock / 2
  return ( ( F_CPU / 2ul ) / ( 1000000ul ) );
}

inline constexpr uint32_t
stepper_timer_ticks_for_us ( uint32_t us_n )
{
  return ( us_n * stepper_timer_ticks_per_us () );
}

void
setup_steppers ()
{
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    steppers[ ii ].reset ();
  }
  stepper_sync_groups_ready.clear ();
  for ( unsigned int ii = 0; ii != ::cnce::router_num_stepper_sync_groups;
        ++ii ) {
    stepper_sync_groups[ ii ].clear ();
  }

  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    stepper_queue_changed[ ii ] = 0;
  }
  pulse_ticks_high_default = stepper_timer_ticks_for_us ( 5 );

  // Axis pulse timer counters
  stepper_timer_setup< 0 > ();
  stepper_timer_setup< 1 > ();
  stepper_timer_setup< 2 > ();
  stepper_timer_setup< 3 > ();
  stepper_timer_setup< 4 > ();
  stepper_timer_setup< 5 > ();
}

void
steppers_abort_and_clear ()
{
  stepper_clear_job_queues< 0 > ();
  stepper_clear_job_queues< 1 > ();
  stepper_clear_job_queues< 2 > ();
  stepper_clear_job_queues< 3 > ();
  stepper_clear_job_queues< 4 > ();
  stepper_clear_job_queues< 5 > ();

  // Clear sync groups
  stepper_sync_groups_ready.clear ();
  for ( unsigned int ii = 0; ii != ::cnce::router_num_stepper_sync_groups;
        ++ii ) {
    stepper_sync_groups[ ii ].clear ();
  }
}

/// @brief Main steppers processing function - Called by the main loop
void
steppers_poll ()
{
  // Feed steppers
  steppers_feed ();

  // Poll sync groups
  while ( !stepper_sync_groups_ready.is_empty () ) {
    ::cnce::Stepper_Sync_Group * sync_group (
        stepper_sync_groups_ready.front () );
    stepper_sync_groups_ready.pop_front_not_empty ();
    // Start after a sync lock completed
    if ( sync_group->num_lock_releases != 0 ) {
      if ( sync_group->all_locked () ) {
        sync_group->num_steppers_sync_locked = 0;
        --sync_group->num_lock_installs;
        --sync_group->num_lock_releases;

        steppers_start_synced ( *sync_group );
      }
    }
  }

  // Send state messages if a stepper queue changed
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    if ( stepper_queue_changed[ ii ] != 0 ) {
      stepper_queue_changed[ ii ] = 0;
      com_request_state_send ( STATE_SEND_STEPPER_0 + ii );
    }
  }
}

void
steppers_feed ()
{
  const unsigned int num_feed_rounds ( 4 );
  for ( unsigned int ii = 0; ii != num_feed_rounds; ++ii ) {
    stepper_feed< 0 > ();
    stepper_feed< 1 > ();
    stepper_feed< 2 > ();
    stepper_feed< 3 > ();
    stepper_feed< 4 > ();
    stepper_feed< 5 > ();
  }
}

void
steppers_start_synced ( ::cnce::Stepper_Sync_Group & sync_group_n )
{
  // Install pulse start blocks and disable sync locks
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    if ( sync_group_n.steppers[ ii ] ) {
      ::cnce::Router_Stepper & stepper ( steppers[ ii ] );
      stepper.pulse_start_block = true;
      stepper.sync_lock_request = false;
      stepper.sync_lock_locked = false;
    }
  }
  // Feed expanded job buffer
  steppers_feed ();
  // Release pulse start blocks
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    if ( sync_group_n.steppers[ ii ] ) {
      ::cnce::Router_Stepper & stepper ( steppers[ ii ] );
      stepper.pulse_start_block = false;
    }
  }

  {
    typedef void ( *FPtr ) ();
    FPtr funcs[::cnce::router_num_steppers * 2 ];
    // Fill function table
    {
      FPtr * func_load ( &funcs[ 0 ] );
      FPtr * func_start ( &funcs[ 0 ] + sync_group_n.num_steppers_registered );
      for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
        if ( sync_group_n.steppers[ ii ] ) {
          switch ( ii ) {
          case 0:
            *func_load = &stepper_start_synced_load_pulses< 0 >;
            *func_start = &stepper_start_synced_start_timers< 0 >;
            break;
          case 1:
            *func_load = &stepper_start_synced_load_pulses< 1 >;
            *func_start = &stepper_start_synced_start_timers< 1 >;
            break;
          case 2:
            *func_load = &stepper_start_synced_load_pulses< 2 >;
            *func_start = &stepper_start_synced_start_timers< 2 >;
            break;
          case 3:
            *func_load = &stepper_start_synced_load_pulses< 3 >;
            *func_start = &stepper_start_synced_start_timers< 3 >;
            break;
          case 4:
            *func_load = &stepper_start_synced_load_pulses< 4 >;
            *func_start = &stepper_start_synced_start_timers< 4 >;
            break;
          case 5:
            *func_load = &stepper_start_synced_load_pulses< 5 >;
            *func_start = &stepper_start_synced_start_timers< 5 >;
            break;
          default:
            --func_load;
            --func_start;
            break;
          };
          ++func_load;
          ++func_start;
        }
      }
    }
    // Call all functions from the table
    {
      FPtr * func_cur ( &funcs[ 0 ] );
      FPtr * func_end ( &funcs[ 0 ] +
                        sync_group_n.num_steppers_registered * 2 );
      while ( func_cur != func_end ) {
        ( *func_cur ) ();
        ++func_cur;
      }
    }
  }
}

template < uint8_t STEPPER_INDEX >
void
stepper_start_synced_load_pulses ()
{
  // typedef adue::tc::Channel_T < STEPPER_INDEX + stepper_timer_offset >
  // FTimer;
  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );

  if ( !stepper.jobs_expanded.is_empty () ) {
    stepper_load_next_pulse< STEPPER_INDEX, false > ();
  }
}

template < uint8_t STEPPER_INDEX >
void
stepper_start_synced_start_timers ()
{
  typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;
  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );
  if ( stepper.pulse_running ) {
    FTimer::clock_enable_start ();
  }
}

template < uint8_t STEPPER_INDEX >
inline void
stepper_clear_job_queues ()
{
  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );
  // Clear incoming job queue
  stepper.jobs_incoming.reset ();
  // Clear expanded job queue
  {
    typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;
    NVIC_DisableIRQ ( FTimer::irqn () );
    stepper.jobs_expanded.reset ();
    NVIC_EnableIRQ ( FTimer::irqn () );
  }

  stepper.pulse_start_block = false;
  stepper.sync_lock_request = false;
  stepper.sync_lock_locked = false;
}

template < uint8_t STEPPER_INDEX >
inline void
stepper_feed ()
{
  typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;

  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );
  if ( !stepper.sync_lock_request ) {
    if ( !stepper.jobs_incoming.is_empty () &&
         !stepper.jobs_expanded.is_full () ) {
      bool push_expanded_job ( true );
      // Prepare new expanded job from incoming job
      {
        ::cnce::Router_Stepper_Job & ijob ( *stepper.jobs_incoming.front () );
        if ( ijob.job_type () != ::cnce::JOB_SYNC_LOCK ) {
          ::cnce::Router_Axis_Job_Expanded & ejob (
              *stepper.jobs_expanded.back () );
          ejob.set_job_type ( ijob.job_type () );
          if ( ejob.is_movement_job () ) {
            // actual usecs
            ejob.set_ticks ( stepper_timer_ticks_for_us ( ijob.usecs () ) );
          } else if ( ejob.is_switch_job () ) {
            // usecs are the switch index
            ejob.set_ticks ( ijob.usecs () );
          } else {
            // Shouldn't happen but be safe anyway
            push_expanded_job = false;
          }
        } else {
          // This is a sync lock. Don't push any more expanded jobs.
          stepper.sync_lock_request = true;
          stepper.stepper_sync_group = ijob.switch_index ();
          push_expanded_job = false;
        }
        stepper.jobs_incoming.pop_front_not_empty ();
      }

      // Push expanded job
      if ( push_expanded_job ) {
        NVIC_DisableIRQ ( FTimer::irqn () );
        stepper.jobs_expanded.push_back_not_full ();
        if ( !stepper.pulse_running && !stepper.pulse_start_block ) {
          stepper_load_next_pulse< STEPPER_INDEX, false > ();
          if ( stepper.pulse_running ) {
            FTimer::clock_enable_start ();
          }
        }
        NVIC_EnableIRQ ( FTimer::irqn () );
      }
    }
  } else {
    if ( !stepper.sync_lock_locked ) {
      if ( stepper.jobs_expanded.is_empty () && !stepper.pulse_running ) {
        stepper.sync_lock_locked = true;
        {
          ::cnce::Stepper_Sync_Group & sync_group (
              stepper_sync_groups[ stepper.stepper_sync_group ] );
          ++sync_group.num_steppers_sync_locked;
          if ( sync_group.all_locked () ) {
            stepper_sync_groups_ready.push_back ( &sync_group );
          }
        }
      }
    }
  }
}

template < uint8_t STEPPER_INDEX, bool USE_TIMER_OFFSET >
inline void
stepper_load_next_pulse ()
{
  typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;
  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );

  bool pulse_job;
  do {
    pulse_job = false;

    const ::cnce::Router_Axis_Job_Expanded & ejob (
        *stepper.jobs_expanded.front () );

    // Number of ticks for the pulse high phase
    uint32_t pulse_ticks_high ( pulse_ticks_high_default );
    {
      uint32_t pulse_ticks_high_max ( ejob.ticks () / 2 );
      if ( pulse_ticks_high > pulse_ticks_high_max ) {
        pulse_ticks_high = pulse_ticks_high_max;
      }
    }
    if ( USE_TIMER_OFFSET ) {
      pulse_ticks_high += FTimer::counter ();
    }

    switch ( ejob.job_type () ) {
    case ::cnce::JOB_MOVE_HOLD:
      FTimer::ra_set ( pulse_ticks_high ); // pulse high phase
      FTimer::rc_set ( ejob.ticks () );    // pulse total length
      pulse_job = true;
      break;
    case ::cnce::JOB_MOVE_FORWARD:
      stepper.pin_dir.set_high ();
      stepper.pin_clock.set_high ();
      FTimer::ra_set ( pulse_ticks_high );
      FTimer::rc_set ( ejob.ticks () );
      pulse_job = true;
      break;
    case ::cnce::JOB_MOVE_BACKWARD:
      stepper.pin_dir.set_low ();
      stepper.pin_clock.set_high ();
      FTimer::ra_set ( pulse_ticks_high );
      FTimer::rc_set ( ejob.ticks () );
      pulse_job = true;
      break;
    case ::cnce::JOB_SWITCH_SET_LOW:
      set_switch_low ( ejob.switch_index () );
      break;
    case ::cnce::JOB_SWITCH_SET_HIGH:
      set_switch_high ( ejob.switch_index () );
      break;
    default:
      break;
    }

    // Pop expanded job from queue
    stepper.jobs_expanded.pop_front ();
  } while ( !pulse_job && !stepper.jobs_expanded.is_empty () );

  stepper.pulse_running = pulse_job;

  // Register a queue change
  stepper_queue_changed[ STEPPER_INDEX ] = 1;
}

template < uint8_t STEPPER_INDEX >
inline void
stepper_timer_interrupt ()
{
  typedef adue::tc::Channel_T< STEPPER_INDEX + stepper_timer_offset > FTimer;

  ::cnce::Router_Stepper & stepper ( steppers[ STEPPER_INDEX ] );
  const uint32_t status ( FTimer::status () );
  if ( ( status & TC_SR_CPAS ) != 0 ) {
    if ( stepper.pulse_running ) {
      stepper.pin_clock.set_low ();
    }
  }
  if ( ( status & TC_SR_CPCS ) != 0 ) {
    if ( stepper.pulse_running ) {
      if ( !stepper.jobs_expanded.is_empty () ) {
        stepper_load_next_pulse< STEPPER_INDEX, true > ();
      } else {
        stepper.pulse_running = false;
        // Register a queue change
        stepper_queue_changed[ STEPPER_INDEX ] = 1;
      }
    }
    if ( !stepper.pulse_running ) {
      FTimer::clock_disable ();
    }
  }
}

// Timer handlers

void
TC3_Handler ()
{
  stepper_timer_interrupt< 0 > ();
}

void
TC4_Handler ()
{
  stepper_timer_interrupt< 1 > ();
}

void
TC5_Handler ()
{
  stepper_timer_interrupt< 2 > ();
}

void
TC6_Handler ()
{
  stepper_timer_interrupt< 3 > ();
}

void
TC7_Handler ()
{
  stepper_timer_interrupt< 4 > ();
}

void
TC8_Handler ()
{
  stepper_timer_interrupt< 5 > ();
}
