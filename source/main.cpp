/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "communication.hpp"
#include "frequency_generators.hpp"
#include "outputs.hpp"
#include "sensors.hpp"
#include "steppers.hpp"
#include "switches.hpp"
#include <adue/pio/leds.hpp>
#include <adue/pmc.hpp>
#include <adue/system_init.hpp>
#include <cnce/router/config.hpp>

// Variables

bool usb_was_connected;
volatile bool slow_event_blink;
uint32_t slow_event_counter_blink;

// Function declaration

void
setup ();

void
loop ();

void
check_slow_events_common ();

void
check_communication ();

void
check_slow_events_connected ();

void
halt_and_reset ();

void
router_system_tick ();

// Function definition

int
main ()
{
  // System setup
  adue::system_init ();

  // User setup
  setup ();

  // Work loop
  while ( true ) {
    loop ();
  }

  return 0;
}

void
setup ()
{
  usb_was_connected = false;
  slow_event_blink = false;
  slow_event_counter_blink = 0;
  com_counter_heartbeat = 0;

  // Enable peripheral clocks
  adue::pmc_set_writeprotect ( false );
  // pio
  adue::pmc_enable_periph_clk ( ID_PIOA );
  adue::pmc_enable_periph_clk ( ID_PIOB );
  adue::pmc_enable_periph_clk ( ID_PIOC );
  adue::pmc_enable_periph_clk ( ID_PIOD );

  adue::pio::init_led_p13 ();
  adue::pio::init_led_rx ();
  adue::pio::init_led_tx ();

  adue::pio::set_led_p13 ( false );
  adue::pio::set_led_rx ( false );
  adue::pio::set_led_tx ( false );

  setup_outputs ();
  setup_frequency_generators ();
  setup_steppers ();
  setup_switches ();
  setup_sensors ();
  setup_communication ();
}

void
loop ()
{
  check_slow_events_common ();
  check_communication ();
  if ( usb_bulk_io.is_connnected () ) {
    check_slow_events_connected ();
    steppers_poll ();

    com_read ();
    steppers_poll ();

    sensors_read ();
    steppers_poll ();

    com_write ();
    steppers_poll ();
  }
}

inline void
check_slow_events_common ()
{
  if ( slow_event_blink ) {
    slow_event_blink = false;
    adue::pio::toggle_led_p13 ();
  }
}

inline void
check_communication ()
{
  // USB connection state changes
  if ( usb_was_connected != usb_bulk_io.is_connnected () ) {
    usb_was_connected = usb_bulk_io.is_connnected ();
    if ( usb_was_connected ) {
      // Reset communication heartbeat counter
      com_counter_income_timeout = false;
      com_counter_income = 0;
      com_request_state_send_all ();
    } else {
      // Disconnected
      halt_and_reset ();
    }
  }
  // Incoming message timeout
  if ( com_counter_income_timeout ) {
    com_counter_income_timeout = false;
    halt_and_reset ();
  }
}

inline void
check_slow_events_connected ()
{
  // State send timeout
  if ( com_counter_heartbeat_timeout ) {
    com_counter_heartbeat_timeout = false;
    com_request_state_send ( STATE_SEND_SENSORS );
    com_request_state_send ( STATE_SEND_SWITCHES );
    for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
      com_request_state_send ( STATE_SEND_STEPPER_0 + ii );
    }
  }
}

void
halt_and_reset ()
{
  steppers_abort_and_clear ();
  sensors_reset ();
  switches_reset ();
  com_reset ();
}

// Timer handlers

void
SysTick_Handler ()
{
  ++slow_event_counter_blink;
  if ( slow_event_counter_blink == 500 ) {
    slow_event_counter_blink = 0;
    slow_event_blink = true;
  }

  ++com_counter_heartbeat;
  if ( com_counter_heartbeat == 500 ) {
    com_counter_heartbeat = 0;
    com_counter_heartbeat_timeout = true;
  }

  ++com_counter_income;
  if ( com_counter_income == 500 ) {
    com_counter_income = 0;
    com_counter_income_timeout = true;
  }

  if ( com_counter_usb_read == 100 ) {
    adue::pio::set_led_rx ( false );
  } else {
    adue::pio::set_led_rx ( true );
    ++com_counter_usb_read;
  }
  if ( com_counter_usb_write == 100 ) {
    adue::pio::set_led_tx ( false );
  } else {
    adue::pio::set_led_tx ( true );
    ++com_counter_usb_write;
  }
}
