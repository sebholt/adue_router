/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include <inttypes.h>

// USB descriptor strings
static const uint8_t usb_desc_str_manu[] = {
    'H', 0, 'T', 0, 'e', 0, 'c', 0, 'h', 0 };
static const uint8_t usb_desc_str_product[] = {
    'C', 0, 'N', 0, 'C', 0, ':', 0, 'P', 0, 'o', 0, 'r', 0, 't', 0, 'a', 0,
    'l', 0, '-', 0, 'R', 0, 'o', 0, 'u', 0, 't', 0, 'e', 0, 'r', 0 };
static const uint8_t usb_desc_str_serial[] = {
    '0', 0, '0', 0, '.', 0, '1', 0, '0', 0 };
