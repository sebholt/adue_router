/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_stepper_sync_group_hpp__
#define __inc_cnce_stepper_sync_group_hpp__

#include <inttypes.h>

namespace cnce
{

class Stepper_Sync_Group
{
  // Public methods
  public:
  void
  clear ();

  void
  register_stepper ( uint8_t stepper_index_n );

  bool
  all_locked () const;

  // Public attributes
  public:
  uint8_t num_steppers_registered;
  uint8_t num_steppers_sync_locked;
  bool steppers[::cnce::router_num_steppers ];
  uint32_t num_lock_installs;
  uint32_t num_lock_releases;
};

inline void
Stepper_Sync_Group::clear ()
{
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    steppers[ ii ] = false;
  }
  num_steppers_registered = 0;
  num_steppers_sync_locked = 0;

  num_lock_installs = 0;
  num_lock_releases = 0;
}

inline void
Stepper_Sync_Group::register_stepper ( uint8_t stepper_index_n )
{
  if ( stepper_index_n < ::cnce::router_num_steppers ) {
    if ( !steppers[ stepper_index_n ] ) {
      steppers[ stepper_index_n ] = true;
      ++num_steppers_registered;
    }
  }
}

inline bool
Stepper_Sync_Group::all_locked () const
{
  return ( num_steppers_registered == num_steppers_sync_locked );
}

} // namespace cnce

#endif
