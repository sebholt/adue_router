/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "outputs.hpp"

// Variables extern

::cnce::Router_Output outputs[::cnce::router_num_outputs ];

// Functions

void
setup_outputs ()
{
  outputs[ 0 ].pin.reset ( PIOB, ( 1 << 21 ) );
  outputs[ 1 ].pin.reset ( PIOB, ( 1 << 14 ) );
  outputs[ 2 ].pin.reset ( PIOC, ( 1 << 15 ) );
  outputs[ 3 ].pin.reset ( PIOC, ( 1 << 14 ) );
  outputs[ 4 ].pin.reset ( PIOC, ( 1 << 17 ) );
  outputs[ 5 ].pin.reset ( PIOC, ( 1 << 16 ) );
  outputs[ 6 ].pin.reset ( PIOC, ( 1 << 19 ) );
  outputs[ 7 ].pin.reset ( PIOC, ( 1 << 18 ) );
  outputs[ 8 ].pin.reset ( PIOA, ( 1 << 19 ) );
  outputs[ 9 ].pin.reset ( PIOA, ( 1 << 20 ) );
  outputs[ 10 ].pin.reset ( PIOD, ( 1 << 10 ) );
  outputs[ 11 ].pin.reset ( PIOC, ( 1 << 9 ) );
  outputs[ 12 ].pin.reset ( PIOD, ( 1 << 9 ) );
  outputs[ 13 ].pin.reset ( PIOA, ( 1 << 7 ) );
  outputs[ 14 ].pin.reset ( PIOD, ( 1 << 3 ) );
  outputs[ 15 ].pin.reset ( PIOD, ( 1 << 6 ) );
  outputs[ 16 ].pin.reset ( PIOD, ( 1 << 1 ) );
  outputs[ 17 ].pin.reset ( PIOD, ( 1 << 2 ) );
  outputs[ 18 ].pin.reset ( PIOA, ( 1 << 15 ) );
  outputs[ 19 ].pin.reset ( PIOD, ( 1 << 0 ) );
  outputs[ 20 ].pin.reset ( PIOB, ( 1 << 26 ) );
  outputs[ 21 ].pin.reset ( PIOA, ( 1 << 14 ) );

  for ( unsigned int ii = 0; ii != ::cnce::router_num_outputs; ++ii ) {
    ::cnce::Router_Output & output ( outputs[ ii ] );
    output.pin.setup ();
    output.user_switch = 0;
    output.user_freq_gen = 0;
    output.user_stepper = 0;
  }
}
