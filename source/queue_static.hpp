/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_queue_static_hpp__
#define __inc_cnce_queue_static_hpp__

#include <inttypes.h>

namespace cnce
{

template < class ETYPE, uint32_t QNUM >
class Queue_Static
{
  // Public methods
  public:
  Queue_Static ();

  void
  reset ();

  uint32_t
  capacity () const;

  uint32_t
  size () const;

  bool
  is_empty () const;

  bool
  is_full () const;

  ETYPE *
  back ();

  const ETYPE *
  back () const;

  void
  push_back_not_full ();

  void
  push_back ();

  ETYPE *
  front ();

  const ETYPE *
  front () const;

  void
  pop_front_not_empty ();

  void
  pop_front ();

  void
  increment_index ( uint32_t & index_n );

  // Private attributes
  private:
  static const uint32_t _capacity = QNUM;
  uint32_t _size;
  uint32_t _front_index;
  uint32_t _back_index;
  ETYPE _jobs[ _capacity ];
};

template < class ETYPE, uint32_t QNUM >
inline Queue_Static< ETYPE, QNUM >::Queue_Static ()
{
  reset ();
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::reset ()
{
  _size = 0;
  _front_index = 0;
  _back_index = 0;
}

template < class ETYPE, uint32_t QNUM >
inline uint32_t
Queue_Static< ETYPE, QNUM >::capacity () const
{
  return _capacity;
}

template < class ETYPE, uint32_t QNUM >
inline uint32_t
Queue_Static< ETYPE, QNUM >::size () const
{
  return _size;
}

template < class ETYPE, uint32_t QNUM >
inline bool
Queue_Static< ETYPE, QNUM >::is_empty () const
{
  return ( size () == 0 );
}

template < class ETYPE, uint32_t QNUM >
inline bool
Queue_Static< ETYPE, QNUM >::is_full () const
{
  return ( size () == capacity () );
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::increment_index ( uint32_t & index_n )
{
  ++index_n;
  index_n %= _capacity;
}

template < class ETYPE, uint32_t QNUM >
inline ETYPE *
Queue_Static< ETYPE, QNUM >::back ()
{
  return &_jobs[ _back_index ];
}

template < class ETYPE, uint32_t QNUM >
inline const ETYPE *
Queue_Static< ETYPE, QNUM >::back () const
{
  return &_jobs[ _back_index ];
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::push_back ()
{
  increment_index ( _back_index );
  if ( is_full () ) {
    _front_index = _back_index;
  } else {
    ++_size;
  }
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::push_back_not_full ()
{
  increment_index ( _back_index );
  ++_size;
}

template < class ETYPE, uint32_t QNUM >
inline ETYPE *
Queue_Static< ETYPE, QNUM >::front ()
{
  return &_jobs[ _front_index ];
}

template < class ETYPE, uint32_t QNUM >
inline const ETYPE *
Queue_Static< ETYPE, QNUM >::front () const
{
  return &_jobs[ _front_index ];
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::pop_front_not_empty ()
{
  increment_index ( _front_index );
  --_size;
}

template < class ETYPE, uint32_t QNUM >
inline void
Queue_Static< ETYPE, QNUM >::pop_front ()
{
  if ( !is_empty () ) {
    increment_index ( _front_index );
    --_size;
  }
}

} // namespace cnce

#endif
