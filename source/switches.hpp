/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_switches_hpp__
#define __inc_router_switches_hpp__

#include "switch.hpp"
#include <adue/pio/pin.hpp>
#include <cnce/router/config.hpp>
#include <inttypes.h>

// Variables extern

extern uint32_t switches_states;
extern uint32_t switches_states_sent;
extern ::cnce::Router_Switch switches[::cnce::router_num_switches ];

// Function declarations

extern void
setup_switches ();

extern void
switches_reset ();

extern void
switches_set_all_low ();

bool
switch_state ( uint32_t switch_index_n );

void
set_switch_low ( uint32_t switch_index_n );

void
set_switch_high ( uint32_t switch_index_n );

void
set_switch_state ( uint32_t switch_index_n, bool state_n );

// Function definitions

inline bool
switch_state ( uint32_t switch_index_n )
{
  return ( ( switches_states & ( 1 << switch_index_n ) ) != 0 );
}

inline void
set_switch_state ( uint32_t switch_index_n, bool state_n )
{
  if ( state_n ) {
    set_switch_high ( switch_index_n );
  } else {
    set_switch_low ( switch_index_n );
  }
}

#endif
