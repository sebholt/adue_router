/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_router_sensors_hpp__
#define __inc_cnce_router_sensors_hpp__

#include <adue/pio/pin.hpp>
#include <cnce/router/config.hpp>
#include <inttypes.h>

// Variables extern

extern uint32_t sensors_states;
extern uint32_t sensors_states_sent;

// Function declarations

extern void
setup_sensors ();

extern void
sensors_reset ();

extern void
sensors_read ();

#endif
