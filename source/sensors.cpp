/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "sensors.hpp"
#include "communication.hpp"
#include <inttypes.h>

// Variables extern

::Pio * sensors_pio;
uint32_t sensors_states;
uint32_t sensors_states_sent;

// Function definitions

void
setup_sensors ()
{
  sensors_pio = PIOC;
  uint32_t sensors_mask = ( 1 << 1 ) | ( 1 << 2 ) | ( 1 << 3 ) | ( 1 << 4 ) |
                          ( 1 << 5 ) | ( 1 << 6 ) | ( 1 << 7 ) | ( 1 << 8 ) |
                          ( 1 << 12 ) | ( 1 << 13 );
  adue::pio::setup_as_input ( sensors_pio, sensors_mask );

  sensors_states = 0;
  sensors_states_sent = 0;
}

void
sensors_reset ()
{
  sensors_states_sent = 0;
}

void
sensors_read ()
{
  {
    const uint32_t values ( sensors_pio->PIO_PDSR );
    uint8_t * sstates ( reinterpret_cast< uint8_t * > ( &sensors_states ) );
    sstates[ 0 ] = ( values >> 1 );
    sstates[ 0 ] &= 0xFF;
    sstates[ 1 ] = ( values >> 12 );
    sstates[ 1 ] &= 0x03;
  }
  if ( sensors_states != sensors_states_sent ) {
    com_request_state_send ( STATE_SEND_SENSORS );
  }
}
