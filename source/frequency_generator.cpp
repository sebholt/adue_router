/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "frequency_generator.hpp"
#include "switch.hpp"

namespace cnce
{

void
Router_Frequency_Generator::invalidate ()
{
  pin.set_mask ( 0 );
  if ( user_switch != 0 ) {
    user_switch->invalidate ();
    user_switch = 0;
  }
  stop ();
}

} // namespace cnce
