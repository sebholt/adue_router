/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_communication_hpp__
#define __inc_router_communication_hpp__

#include "usb.hpp"
#include <cnce/router/config.hpp>

// Types

enum State_Send_Type
{
  STATE_SEND_STEPPER_0,

  STATE_SEND_SENSORS = STATE_SEND_STEPPER_0 + ::cnce::router_num_steppers,
  STATE_SEND_SWITCHES,

  STATE_SEND_NUM_TYPES
};

// Variables

extern volatile bool com_counter_heartbeat_timeout;
extern volatile bool com_counter_income_timeout;
extern volatile uint32_t com_counter_heartbeat;
extern volatile uint32_t com_counter_income;

extern volatile uint32_t com_counter_usb_read;
extern volatile uint32_t com_counter_usb_write;

extern uint16_t com_latest_message_uid_incoming;
extern uint16_t com_latest_message_uid_outgoing;

// Functions

extern void
setup_communication ();

extern void
com_reset ();

extern void
com_request_state_send ( uint8_t state_send_type_n );

extern void
com_request_state_send_all ();

extern void
com_read ();

extern void
com_write ();

#endif
