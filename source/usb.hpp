/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_usb_hpp__
#define __inc_router_usb_hpp__

#include <adue/usb/usb_bulk_io.hpp>

// USB instance
extern adue::USB_Bulk_IO usb_bulk_io;

extern void
setup_usb ();

#endif
