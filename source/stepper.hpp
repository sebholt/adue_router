/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_router_stepper_hpp__
#define __inc_cnce_router_stepper_hpp__

#include "queue_static.hpp"
#include "stepper_job.hpp"
#include "stepper_job_expanded.hpp"
#include <adue/pio/pin.hpp>
#include <cnce/router/config.hpp>

namespace cnce
{

class Router_Stepper
{
  // Public methods
  public:
  void
  reset ();

  uint32_t
  jobs_total ();

  // Public attributes
  public:
  bool pulse_running;
  bool pulse_start_block;
  adue::pio::Pin_Output pin_dir;
  adue::pio::Pin_Output pin_clock;
  ::cnce::Queue_Static< ::cnce::Router_Axis_Job_Expanded, 16 > jobs_expanded;

  /// @brief Keeps some memory distance between interrupt accessed variables and
  /// main thread variables
  uint32_t mem_spacer_dummy;

  bool sync_lock_request;
  bool sync_lock_locked;
  uint8_t stepper_sync_group;
  ::cnce::Queue_Static< ::cnce::Router_Stepper_Job,
                        ::cnce::router_stepper_queue_capacity >
      jobs_incoming;
};

inline uint32_t
Router_Stepper::jobs_total ()
{
  return ( jobs_expanded.size () + jobs_incoming.size () );
}

} // namespace cnce

#endif
