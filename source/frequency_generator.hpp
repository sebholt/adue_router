/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_frequency_generator_hpp__
#define __inc_router_frequency_generator_hpp__

#include <adue/pio/pin.hpp>
#include <cnce/router/config.hpp>
#include <inttypes.h>

namespace cnce
{

// Forward declaration
class Router_Switch;

class Router_Frequency_Generator
{
  // Public methods
  public:
  typedef void ( *Call_Func ) ();
  typedef void ( *Frequency_Func ) ( uint32_t usecs_high_n,
                                     uint32_t usecs_total_n );

  // Public methods
  public:
  void
  invalidate ();

  void
  start ();

  void
  stop ();

  void
  set_frequency ( uint32_t usecs_high_n, uint32_t usecs_total_n );

  // Private methods
  private:
  // Public attributes
  public:
  adue::pio::Pin_Output pin;
  bool is_running;
  bool frequency_change;
  uint32_t usecs_high;
  uint32_t usecs_total;

  Frequency_Func frequency_func;
  Call_Func start_func;
  Call_Func stop_func;

  ::cnce::Router_Switch * user_switch;
};

inline void
Router_Frequency_Generator::start ()
{
  start_func ();
}

inline void
Router_Frequency_Generator::stop ()
{
  stop_func ();
}

inline void
Router_Frequency_Generator::set_frequency ( uint32_t usecs_high_n,
                                            uint32_t usecs_total_n )
{
  frequency_func ( usecs_high_n, usecs_total_n );
}

} // namespace cnce

#endif
