/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "communication.hpp"
#include "frequency_generators.hpp"
#include "outputs.hpp"
#include "sensors.hpp"
#include "steppers.hpp"
#include "switches.hpp"
#include "usb.hpp"
#include <adue/ring_buffer.hpp>
#include <adue/serialize.hpp>
#include <cnce/frog/messages_in.hpp>
#include <cnce/frog/messages_out.hpp>
#include <cnce/router/config.hpp>

// Variables extern

bool com_state_send_register[ STATE_SEND_NUM_TYPES ];
adue::Ring_Buffer< uint8_t, STATE_SEND_NUM_TYPES > com_state_send_ring;

volatile bool com_counter_heartbeat_timeout;
volatile bool com_counter_income_timeout;
volatile uint32_t com_counter_heartbeat;
volatile uint32_t com_counter_income;

volatile uint32_t com_counter_usb_read;
volatile uint32_t com_counter_usb_write;

uint16_t com_latest_message_uid_incoming;
uint16_t com_latest_message_uid_outgoing;

// Types static

typedef void ( *Message_Out_Handler ) ();
typedef void ( *Message_Sender ) ();

// Variables static

static uint8_t message_sizes[::cnce::frog_message_out_num_types ];
static Message_Out_Handler
    message_handlers[::cnce::frog_message_out_num_types ];

static const uint32_t read_buffer_size ( 128 );
static uint8_t read_buffer[ read_buffer_size ];
static uint32_t read_buffer_pos;
static uint32_t read_msg_size;
static bool read_header_found;

uint8_t * send_data;
uint32_t send_data_size;

static ::cnce::Frog_Message_In_State_Stepper msg_state_stepper;
static ::cnce::Frog_Message_In_State_Sensors_32 msg_state_sensors;
static ::cnce::Frog_Message_In_State_Switches_32 msg_state_switches;

static Message_Sender message_senders[ (uint32_t)STATE_SEND_NUM_TYPES ];

// Function declarations

void
handle_incoming_message ();

void
handle_message_null ();

void
handle_message_heartbeat ();

void
handle_message_steps_u16_64 ();

void
handle_message_stepper_sync_config ();

void
handle_message_stepper_sync ();

void
handle_message_switch ();

void
handle_message_output_config ();

void
handle_message_generator_frequency ();

void
message_uid_outgoing_increment ();

template < uint8_t STEPPER_INDEX >
void
send_state_stepper ();

void
send_state_sensors ();

void
send_state_switches ();

// Function definitions

void
setup_communication ()
{
  for ( unsigned int ii = 0; ii != STATE_SEND_NUM_TYPES; ++ii ) {
    com_state_send_register[ ii ] = false;
  }
  com_state_send_ring.reset ();

  com_counter_heartbeat_timeout = false;
  com_counter_income_timeout = false;
  com_counter_heartbeat = 0;
  com_counter_income = 0;

  com_counter_usb_read = 0;
  com_counter_usb_write = 0;

  com_latest_message_uid_incoming = 0;
  com_latest_message_uid_outgoing = 0;

  // Init message sizes
  for ( uint32_t ii = 0; ii != ::cnce::frog_message_out_num_types; ++ii ) {
    message_sizes[ ii ] = 0;
  }
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_HEARTBEAT ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Heartbeat );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPS_U16_64 ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Steps_U16_64 );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC_CONFIG ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Stepper_Sync_Config );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Stepper_Sync );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_SWITCH ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Switch );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_OUTPUT_CONFIG ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Output_Config );
  message_sizes[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_GENERATOR_FREQUENCY ) ] =
      sizeof ( ::cnce::Frog_Message_Out_Generator_Frequency );

  // Init message handlers
  for ( uint32_t ii = 0; ii != ::cnce::frog_message_out_num_types; ++ii ) {
    message_handlers[ ii ] = &handle_message_null;
  }
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_HEARTBEAT ) ] = &handle_message_heartbeat;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPS_U16_64 ) ] = &handle_message_steps_u16_64;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC_CONFIG ) ] =
      &handle_message_stepper_sync_config;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC ) ] = &handle_message_stepper_sync;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_SWITCH ) ] = &handle_message_switch;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_OUTPUT_CONFIG ) ] =
      &handle_message_output_config;
  message_handlers[::cnce::frog_message_out_type_index (
      ::cnce::FROG_MESSAGE_OUT_GENERATOR_FREQUENCY ) ] =
      &handle_message_generator_frequency;

  read_buffer_pos = 0;
  read_msg_size = 0;
  read_header_found = false;

  // Message sending

  send_data = 0;
  send_data_size = 0;

  msg_state_stepper.init_header ();
  msg_state_sensors.init_header ();
  msg_state_switches.init_header ();

  msg_state_stepper.reset ();
  msg_state_sensors.reset ();
  msg_state_switches.reset ();

  // Message send functions
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 0 ] =
      &send_state_stepper< 0 >;
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 1 ] =
      &send_state_stepper< 1 >;
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 2 ] =
      &send_state_stepper< 2 >;
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 3 ] =
      &send_state_stepper< 3 >;
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 4 ] =
      &send_state_stepper< 4 >;
  message_senders[ (uint32_t)STATE_SEND_STEPPER_0 + 5 ] =
      &send_state_stepper< 5 >;
  message_senders[ (uint32_t)STATE_SEND_SENSORS ] = &send_state_sensors;
  message_senders[ (uint32_t)STATE_SEND_SWITCHES ] = &send_state_switches;

  setup_usb ();
}

void
com_reset ()
{
  // Incoming
  read_buffer_pos = 0;
  read_msg_size = 0;
  read_header_found = false;

  // Sending
  for ( unsigned int ii = 0; ii != STATE_SEND_NUM_TYPES; ++ii ) {
    com_state_send_register[ ii ] = false;
  }
  com_state_send_ring.clear ();

  send_data = 0;
  send_data_size = 0;

  com_latest_message_uid_incoming = 0;
  com_latest_message_uid_outgoing = 0;
}

void
com_request_state_send ( uint8_t state_send_type_n )
{
  bool & state_reg ( com_state_send_register[ state_send_type_n ] );
  if ( !state_reg ) {
    state_reg = true;
    com_state_send_ring.push_back_not_full ( state_send_type_n );
  }
}

void
com_request_state_send_all ()
{
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    com_request_state_send ( STATE_SEND_STEPPER_0 + ii );
  }
  com_request_state_send ( STATE_SEND_SENSORS );
  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
com_read ()
{
  if ( usb_bulk_io.read_bank_poll () ) {
    volatile uint8_t * rptr ( usb_bulk_io.read_bank () );
    uint32_t bytes_avail ( usb_bulk_io.read_bank_bytes () );
    if ( bytes_avail != 0 ) {

      com_counter_usb_read = 0;

      do {
        const uint8_t rbyte ( *rptr );
        ++rptr;
        --bytes_avail;

        bool good_byte ( read_header_found );
        if ( !read_header_found ) {
          do {
            if ( read_buffer_pos < ::cnce::frog_message_out_header_size ) {
              // Message id byte
              if ( rbyte ==
                   ::cnce::frog_message_out_header[ read_buffer_pos ] ) {
                good_byte = true;
                break;
              } else {
                // Invalid header id byte
                if ( read_buffer_pos != 0 ) {
                  // Try byte again as first header byte
                  read_buffer_pos = 0;
                } else {
                  // First byte invalid; discard it
                  break;
                }
              }
            } else {
              // Message type byte
              if ( ::cnce::frog_message_out_type_valid ( rbyte ) ) {
                // Valid incoming message type
                good_byte = true;
                read_msg_size =
                    message_sizes[::cnce::frog_message_out_type_index (
                        rbyte ) ];
                read_header_found = true;
                break;
              } else {
                // Try byte again as first header byte
                read_buffer_pos = 0;
              }
            }
          } while ( true );
        }
        if ( good_byte ) {
          read_buffer[ read_buffer_pos ] = rbyte;
          ++read_buffer_pos;

          if ( read_buffer_pos == read_msg_size ) {
            // Evaluate complete message
            handle_incoming_message ();
            // Reset parsing state
            read_buffer_pos = 0;
            read_msg_size = 0;
            read_header_found = false;
          }
        }
      } while ( bytes_avail != 0 );
    }

    usb_bulk_io.read_bank_clear ();
  }
}

inline void
handle_incoming_message ()
{
  // Reset com heartbeat counter
  com_counter_income = 0;
  com_counter_income_timeout = false;

  if ( com_latest_message_uid_outgoing == 0 ) {
    // No state was sent so far
    // Chances are there was a heartbeat timeout
    com_request_state_send_all ();
  }

  // Call message handler
  {
    Message_Out_Handler handler;
    {
      const uint32_t handler_index ( ::cnce::frog_message_out_type_index (
          read_buffer[::cnce::frog_message_out_header_size ] ) );
      handler = message_handlers[ handler_index ];
    }
    handler ();
  }
  // Keep latest message uid
  {
    const ::cnce::Frog_Message_Out * message (
        reinterpret_cast< ::cnce::Frog_Message_Out * > ( &read_buffer[ 0 ] ) );
    adue::get_uint16_8le ( com_latest_message_uid_incoming,
                           &message->message_uid[ 0 ] );
  }
}

void
handle_message_null ()
{
  // Dummy
}

void
handle_message_heartbeat ()
{
  // Send a reply
  for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
    com_request_state_send ( STATE_SEND_STEPPER_0 + ii );
  }
}

void
handle_message_steps_u16_64 ()
{
  typedef ::cnce::Frog_Message_Out_Steps_U16_64 MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  bool msg_valid ( true );
  if ( message->stepper_index >= ::cnce::router_num_steppers ) {
    msg_valid = false;
  }
  if ( !::cnce::frog_step_type_valid ( message->step_type ) ) {
    msg_valid = false;
  }
  if ( msg_valid ) {
    ::cnce::Router_Stepper & acon ( steppers[ message->stepper_index ] );
    const uint32_t num_values_max ( message->num_usecs );
    const uint8_t * rptr ( &message->usecs[ 0 ] );
    if ( ::cnce::router_stepper_job_type_is_movement ( message->step_type ) ) {
      // Movement job
      for ( uint32_t ii = 0; ii != num_values_max; ++ii ) {
        uint16_t usecs;
        adue::get_uint16_8le ( usecs, rptr );
        if ( usecs == 0 ) {
          break;
        }
        // Deal with too short pulses
        if ( usecs < ::cnce::router_pulse_usecs_min ) {
          usecs = ::cnce::router_pulse_usecs_min;
        }
        {
          ::cnce::Router_Stepper_Job & ijob ( *acon.jobs_incoming.back () );
          ijob.set_job_type ( message->step_type );
          ijob.set_usecs ( usecs );
          acon.jobs_incoming.push_back ();
        }
        rptr += 2;
      }
    } else if ( ::cnce::router_stepper_job_type_is_switch (
                    message->step_type ) ) {
      // Switch job
      for ( uint32_t ii = 0; ii != num_values_max; ++ii ) {
        uint16_t usecs;
        adue::get_uint16_8le ( usecs, rptr );
        if ( usecs == 0 ) {
          break;
        }
        // Mask high byte value to 0 to get the actual switch index
        usecs &= 0x00ff;
        {
          ::cnce::Router_Stepper_Job & ijob ( *acon.jobs_incoming.back () );
          ijob.set_job_type ( message->step_type );
          ijob.set_switch_index ( usecs );
          acon.jobs_incoming.push_back ();
        }
        rptr += 2;
      }
    }

    com_request_state_send ( STATE_SEND_STEPPER_0 + message->stepper_index );
  } else {
    com_request_state_send ( STATE_SEND_STEPPER_0 );
  }
}

void
handle_message_stepper_sync_config ()
{
  typedef ::cnce::Frog_Message_Out_Stepper_Sync_Config MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  if ( message->sync_group_index < ::cnce::router_num_stepper_sync_groups ) {
    ::cnce::Stepper_Sync_Group & sync_group (
        stepper_sync_groups[ message->sync_group_index ] );
    sync_group.clear ();
    // Install sync locks in all steppers
    for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
      const uint8_t * ptr ( &message->stepper_register[ 0 ] + ( ii / 8 ) );
      if ( ( *ptr & ( uint8_t ( 1 ) << ( ii % 8 ) ) ) != 0 ) {
        sync_group.register_stepper ( ii );
      }
    }
  }

  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
handle_message_stepper_sync ()
{
  typedef ::cnce::Frog_Message_Out_Stepper_Sync MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  if ( message->sync_group_index < ::cnce::router_num_stepper_sync_groups ) {
    ::cnce::Stepper_Sync_Group & sync_group (
        stepper_sync_groups[ message->sync_group_index ] );
    if ( sync_group.num_steppers_registered != 0 ) {
      if ( message->sync_type == ::cnce::FROG_STEPPER_SYNC_LOCK_INSTALL ) {
        // Install sync locks in all registered steppers
        for ( unsigned int ii = 0; ii != ::cnce::router_num_steppers; ++ii ) {
          if ( sync_group.steppers[ ii ] ) {
            ::cnce::Router_Stepper & stepper ( steppers[ ii ] );
            {
              ::cnce::Router_Stepper_Job & ijob (
                  *stepper.jobs_incoming.back () );
              ijob.set_job_type ( ::cnce::JOB_SYNC_LOCK );
              ijob.set_stepper_sync_group_index ( message->sync_group_index );
              stepper.jobs_incoming.push_back ();
            }
          }
        }
        ++sync_group.num_lock_installs;
      } else if ( message->sync_type ==
                  ::cnce::FROG_STEPPER_SYNC_LOCK_RELEASE ) {
        // Increment the sync lock release count if reasonable
        if ( sync_group.num_lock_releases < sync_group.num_lock_installs ) {
          ++sync_group.num_lock_releases;
        }
      }
    }
  }

  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
handle_message_switch ()
{
  typedef ::cnce::Frog_Message_Out_Switch MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  if ( message->switch_index < ::cnce::router_num_switches ) {
    set_switch_state ( message->switch_index, message->switch_state );
  }

  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
handle_message_output_config ()
{
  typedef ::cnce::Frog_Message_Out_Output_Config MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  if ( message->output_index < ::cnce::router_num_outputs ) {
    ::cnce::Router_Output & output ( outputs[ message->output_index ] );
    output.invalidate_users ();
    switch ( message->output_type ) {
    case ::cnce::FROG_OUTPUT_SWITCH_PLAIN:
      if ( message->stepper_switch_index < ::cnce::router_num_switches ) {
        ::cnce::Router_Switch & rswitch (
            switches[ message->stepper_switch_index ] );
        rswitch.invalidate ();
        rswitch.pin = output.pin;
        output.user_switch = &rswitch;
      }
      break;
    case ::cnce::FROG_OUTPUT_SWITCH_FREQUENCY:
      if ( ( message->stepper_switch_index < ::cnce::router_num_switches ) &&
           ( message->freq_gen_index <
             ::cnce::router_num_frequency_generators ) ) {
        ::cnce::Router_Switch & rswitch (
            switches[ message->stepper_switch_index ] );
        ::cnce::Router_Frequency_Generator & freq_gen (
            frequency_generators[ message->freq_gen_index ] );
        rswitch.invalidate ();
        freq_gen.invalidate ();
        freq_gen.pin = output.pin;
        freq_gen.user_switch = &rswitch;
        rswitch.freq_gen = &freq_gen;
        rswitch.pin = output.pin;
        output.user_switch = &rswitch;
        output.user_freq_gen = &freq_gen;
      }
      break;
    case ::cnce::FROG_OUTPUT_STEPPER_DIRECTION:
      if ( message->stepper_switch_index < ::cnce::router_num_steppers ) {
        ::cnce::Router_Stepper & stepper (
            steppers[ message->stepper_switch_index ] );
        stepper.pin_dir.set_mask ( 0 );
        stepper.pin_dir = output.pin;
        output.user_stepper = &stepper;
      }
      break;
    case ::cnce::FROG_OUTPUT_STEPPER_CLOCK:
      if ( message->stepper_switch_index < ::cnce::router_num_steppers ) {
        ::cnce::Router_Stepper & stepper (
            steppers[ message->stepper_switch_index ] );
        stepper.pin_clock.set_mask ( 0 );
        stepper.pin_clock = output.pin;
        output.user_stepper = &stepper;
      }
      break;
    default:
      break;
    }
  }

  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
handle_message_generator_frequency ()
{
  typedef ::cnce::Frog_Message_Out_Generator_Frequency MType;
  const MType * message ( reinterpret_cast< MType * > ( &read_buffer[ 0 ] ) );

  if ( message->generator_index < ::cnce::router_num_frequency_generators ) {
    ::cnce::Router_Frequency_Generator & fgen (
        frequency_generators[ message->generator_index ] );
    uint32_t usecs_high ( 0 );
    uint32_t usecs_total ( 0 );
    adue::get_uint32_8le ( usecs_high, &message->pulse_usecs_high[ 0 ] );
    adue::get_uint32_8le ( usecs_total, &message->pulse_usecs_total[ 0 ] );
    fgen.set_frequency ( usecs_high, usecs_total );
  }

  com_request_state_send ( STATE_SEND_SWITCHES );
}

void
com_write ()
{
  // Request switch state send on demand
  if ( switches_states != switches_states_sent ) {
    com_request_state_send ( STATE_SEND_SWITCHES );
  }

  bool data_written ( false );
  do {
    if ( send_data_size != 0 ) {
      if ( usb_bulk_io.write_bank_available () ) {
        uint32_t num =
            usb_bulk_io.write_some ( send_data, send_data_size, false );
        if ( num != 0 ) {
          send_data += num;
          send_data_size -= num;
          data_written = true;
        } else {
          break;
        }
      } else {
        break;
      }
    } else if ( !com_state_send_ring.is_empty () ) {
      // Reset state send counters
      com_counter_heartbeat = 0;
      {
        const uint8_t state_index ( com_state_send_ring.front () );
        com_state_send_ring.pop_front ();
        com_state_send_register[ state_index ] = false;
        // Call message generator
        message_senders[ state_index ]();
      }
      // Update message uids
      {
        typedef ::cnce::Frog_Message_In_State MType;
        MType * message ( reinterpret_cast< MType * > ( send_data ) );
        // Acquire message uid
        message_uid_outgoing_increment ();
        adue::set_8le_uint16 ( &message->message_uid[ 0 ],
                               com_latest_message_uid_outgoing );
        // Latest incoming message uid
        adue::set_8le_uint16 ( &message->latest_message_uid[ 0 ],
                               com_latest_message_uid_incoming );
      }
    } else {
      break;
    }
  } while ( true );

  if ( data_written ) {
    usb_bulk_io.write_flush_on_demand ();
    com_counter_usb_write = 0;
  }
}

inline void
message_uid_outgoing_increment ()
{
  ++com_latest_message_uid_outgoing;
  if ( com_latest_message_uid_outgoing == 0 ) {
    ++com_latest_message_uid_outgoing;
  }
}

template < uint8_t STEPPER_INDEX >
void
send_state_stepper ()
{
  ::cnce::Frog_Message_In_State_Stepper & message ( msg_state_stepper );
  message.stepper_index = STEPPER_INDEX;
  // Update state message struct
  {
    // Steps queue lengths
    uint16_t njobs ( steppers[ STEPPER_INDEX ].jobs_total () );
    if ( steppers[ STEPPER_INDEX ].pulse_running ) {
      ++njobs;
    }
    adue::set_8le_uint16 ( &message.steps_queue_length[ 0 ], njobs );
  }
  send_data = reinterpret_cast< uint8_t * > ( &message );
  send_data_size = sizeof ( message );
}

void
send_state_sensors ()
{
  {
    ::cnce::Frog_Message_In_State_Sensors_32 & message ( msg_state_sensors );

    // Update state message struct
    adue::set_8le_uint32 ( &message.sensors_states[ 0 ], sensors_states );

    send_data = reinterpret_cast< uint8_t * > ( &message );
    send_data_size = sizeof ( message );
  }
  sensors_states_sent = sensors_states;
}

void
send_state_switches ()
{
  ::cnce::Frog_Message_In_State_Switches_32 & message ( msg_state_switches );

  // Update state message struct
  adue::set_8le_uint32 ( &message.switches_states[ 0 ], switches_states );
  // Remember sent state
  switches_states_sent = switches_states;

  send_data = reinterpret_cast< uint8_t * > ( &message );
  send_data_size = sizeof ( message );
}
