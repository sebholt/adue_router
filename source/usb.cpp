/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "usb.hpp"

// USB instance
adue::USB_Bulk_IO usb_bulk_io;

// USB descriptor strings
#ifdef USB_DESC_COIL_BENCH
#include "usb_desc_coil_bench.hpp"
#endif
#ifdef USB_DESC_PORTAL_ROUTER
#include "usb_desc_portal_router.hpp"
#endif

void
setup_usb ()
{
  // USB init & enable
  usb_bulk_io.init ();
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_MANUFACTURER,
                                usb_desc_str_manu,
                                sizeof ( usb_desc_str_manu ) );
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_PRODUCT,
                                usb_desc_str_product,
                                sizeof ( usb_desc_str_product ) );
  usb_bulk_io.set_desc_string ( adue::USB_Bulk_IO::SI_SERIAL,
                                usb_desc_str_serial,
                                sizeof ( usb_desc_str_serial ) );

  usb_bulk_io.enable ();
}
