/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_steppers_hpp__
#define __inc_router_steppers_hpp__

#include "stepper.hpp"
#include "stepper_sync_group.hpp"
#include <adue/ring_buffer.hpp>
#include <cnce/router/config.hpp>

// Variables

extern ::cnce::Router_Stepper steppers[::cnce::router_num_steppers ];
extern adue::Ring_Buffer< ::cnce::Stepper_Sync_Group *,
                          ::cnce::router_num_stepper_sync_groups >
    stepper_sync_groups_ready;
extern ::cnce::Stepper_Sync_Group
    stepper_sync_groups[::cnce::router_num_stepper_sync_groups ];

// Functions

extern void
setup_steppers ();

extern void
steppers_abort_and_clear ();

extern void
steppers_poll ();

#endif
