/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "output.hpp"
#include "frequency_generator.hpp"
#include "stepper.hpp"
#include "switch.hpp"

namespace cnce
{

void
Router_Output::invalidate_users ()
{
  if ( user_switch != 0 ) {
    user_switch->invalidate ();
    user_switch = 0;
  }
  if ( user_freq_gen != 0 ) {
    user_freq_gen->invalidate ();
    user_freq_gen = 0;
  }
  if ( user_stepper != 0 ) {
    if ( user_stepper->pin_dir == pin ) {
      user_stepper->pin_dir.set_mask ( 0 );
    }
    if ( user_stepper->pin_clock == pin ) {
      user_stepper->pin_clock.set_mask ( 0 );
    }
    user_stepper = 0;
  }
  pin.set_low ();
}

} // namespace cnce
