/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "frequency_generators.hpp"
#include <adue/pmc.hpp>
#include <adue/tc/channel.hpp>

// Variables extern

::cnce::Router_Frequency_Generator
    frequency_generators[::cnce::router_num_frequency_generators ];

// Function declarations

// Function definitions

inline constexpr uint32_t
frequency_generator_ticks_per_us ()
{
  // The selected TIMER_CLOCK1 is the main clock / 2
  return ( ( F_CPU / 2ul ) / ( 1000000ul ) );
}

inline constexpr uint32_t
frequency_generator_ticks_for_us ( uint32_t us_n )
{
  return ( us_n * frequency_generator_ticks_per_us () );
}

template < uint8_t FGEN_IDX >
void
frequency_generator_start ()
{
  typedef adue::tc::Channel_T< FGEN_IDX > FTimer;
  ::cnce::Router_Frequency_Generator & fgen (
      frequency_generators[ FGEN_IDX ] );

  if ( !fgen.is_running ) {
    if ( fgen.usecs_total >= ::cnce::router_frequency_generator_usecs_min ) {
      // Install frequency on demand
      if ( fgen.frequency_change ) {
        fgen.frequency_change = false;
        FTimer::ra_set ( frequency_generator_ticks_for_us ( fgen.usecs_high ) );
        FTimer::rc_set (
            frequency_generator_ticks_for_us ( fgen.usecs_total ) );
      }
      fgen.is_running = true;
      fgen.pin.set_high ();
      FTimer::clock_enable_start ();
    }
  }
}

template < uint8_t FGEN_IDX >
void
frequency_generator_stop ()
{
  typedef adue::tc::Channel_T< FGEN_IDX > FTimer;
  ::cnce::Router_Frequency_Generator & fgen (
      frequency_generators[ FGEN_IDX ] );

  if ( fgen.is_running ) {
    FTimer::clock_disable ();
    fgen.pin.set_low ();
    fgen.is_running = false;
  }
}

template < uint8_t FGEN_IDX >
void
frequency_generator_adjust_frequency ( uint32_t usecs_high_n,
                                       uint32_t usecs_total_n )
{
  typedef adue::tc::Channel_T< FGEN_IDX > FTimer;
  ::cnce::Router_Frequency_Generator & fgen (
      frequency_generators[ FGEN_IDX ] );

  if ( usecs_total_n < usecs_high_n ) {
    usecs_total_n = usecs_high_n;
  }
  if ( usecs_total_n < ::cnce::router_frequency_generator_usecs_min ) {
    // Invalid pulse time, stop generator
    frequency_generator_stop< FGEN_IDX > ();
  }
  {
    // Register frequency change
    NVIC_DisableIRQ ( FTimer::irqn () );
    fgen.usecs_high = usecs_high_n;
    fgen.usecs_total = usecs_total_n;
    fgen.frequency_change = true;
    NVIC_EnableIRQ ( FTimer::irqn () );
  }
}

template < uint8_t FGEN_IDX >
inline void
setup_frequency_generator ()
{
  typedef adue::tc::Channel_T< FGEN_IDX > FTimer;
  ::cnce::Router_Frequency_Generator & fgen (
      frequency_generators[ FGEN_IDX ] );

  fgen.is_running = false;
  fgen.frequency_change = false;
  fgen.usecs_high = 250 * 1000;
  fgen.usecs_total = 500 * 1000;

  fgen.frequency_func = &frequency_generator_adjust_frequency< FGEN_IDX >;
  fgen.start_func = &frequency_generator_start< FGEN_IDX >;
  fgen.stop_func = &frequency_generator_stop< FGEN_IDX >;

  adue::pmc_set_writeprotect ( false );
  adue::pmc_enable_periph_clk ( FTimer::peripheral_id () );

  {
    // Disable TC clock / Stop
    FTimer::clock_disable ();
    // Disable interrupts
    FTimer::interrupt_disable ( 0xFFFFFFFF );
    // Clear status register
    FTimer::status ();
    // Init match registers
    FTimer::ra_set ( frequency_generator_ticks_for_us ( fgen.usecs_high ) );
    FTimer::rb_set ( 0xFFFFFFFF );
    FTimer::rc_set ( frequency_generator_ticks_for_us ( fgen.usecs_total ) );
    // Set mode
    FTimer::set_mode ( TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC |
                       TC_CMR_TCCLKS_TIMER_CLOCK1 );
    // Enable interrupts
    FTimer::interrupt_enable_ra ();
    FTimer::interrupt_enable_rc ();
  }

  // Enable interrupt controller interrupts
  NVIC_EnableIRQ ( FTimer::irqn () );
}

void
setup_frequency_generators ()
{
  setup_frequency_generator< 0 > ();
  setup_frequency_generator< 1 > ();
  setup_frequency_generator< 2 > ();
}

// Timer handlers

template < uint8_t FGEN_IDX >
inline void
frequency_generator_tc_handler ()
{
  typedef adue::tc::Channel_T< FGEN_IDX > FTimer;
  ::cnce::Router_Frequency_Generator & fgen (
      frequency_generators[ FGEN_IDX ] );

  const uint32_t status ( FTimer::status () );
  if ( ( status & TC_SR_CPAS ) != 0 ) {
    fgen.pin.set_low ();
  }
  if ( ( status & TC_SR_CPCS ) != 0 ) {
    if ( fgen.frequency_change ) {
      fgen.frequency_change = false;
      FTimer::ra_set ( frequency_generator_ticks_for_us ( fgen.usecs_high ) );
      FTimer::rc_set ( frequency_generator_ticks_for_us ( fgen.usecs_total ) );
    }
    fgen.pin.set_high ();
  }
}

void
TC0_Handler ()
{
  frequency_generator_tc_handler< 0 > ();
}

void
TC1_Handler ()
{
  frequency_generator_tc_handler< 1 > ();
}

void
TC2_Handler ()
{
  frequency_generator_tc_handler< 2 > ();
}
