/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_io_pins_hpp__
#define __inc_router_io_pins_hpp__

#include "output.hpp"
#include <cnce/router/config.hpp>

// Variables

extern ::cnce::Router_Output outputs[::cnce::router_num_outputs ];

// Functions

extern void
setup_outputs ();

#endif
