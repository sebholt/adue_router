/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_router_stepper_job_type_hpp__
#define __inc_cnce_router_stepper_job_type_hpp__

#include <inttypes.h>

namespace cnce
{

enum Stepper_Job_Type
{
  // Invalid job type
  JOB_TYPE_NULL,

  // Movement
  JOB_MOVE_HOLD,
  JOB_MOVE_FORWARD,
  JOB_MOVE_BACKWARD,

  // Switch
  JOB_SWITCH_SET_LOW,
  JOB_SWITCH_SET_HIGH,

  // Synchronization
  JOB_SYNC_LOCK,

  JOB_TYPE_LIST_END
};

inline bool
router_stepper_job_type_valid ( uint8_t type_n )
{
  return ( type_n > ::cnce::JOB_TYPE_NULL ) &&
         ( type_n < ::cnce::JOB_TYPE_LIST_END );
}

inline bool
router_stepper_job_type_is_movement ( uint8_t type_n )
{
  return ( type_n >= ::cnce::JOB_MOVE_HOLD ) &&
         ( type_n <= ::cnce::JOB_MOVE_BACKWARD );
}

inline bool
router_stepper_job_type_is_switch ( uint8_t type_n )
{
  return ( type_n == ::cnce::JOB_SWITCH_SET_LOW ) ||
         ( type_n == ::cnce::JOB_SWITCH_SET_HIGH );
}

inline bool
router_stepper_job_type_is_sync_lock ( uint8_t type_n )
{
  return ( type_n == ::cnce::JOB_SYNC_LOCK );
}

} // namespace cnce

#endif
