
#include "stepper.hpp"

namespace cnce
{

void
Router_Stepper::reset ()
{
  pulse_running = false;
  pulse_start_block = false;

  sync_lock_request = false;
  sync_lock_locked = false;
  stepper_sync_group = 0;

  jobs_expanded.reset ();
  jobs_incoming.reset ();
}

} // namespace cnce
