/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_switch_hpp__
#define __inc_router_switch_hpp__

#include <adue/pio/pin.hpp>
#include <inttypes.h>

namespace cnce
{

// Forward declaration
class Router_Frequency_Generator;

class Router_Switch
{
  // Public methods
  public:
  Router_Switch ();

  void
  invalidate ();

  void
  set_low ();

  void
  set_high ();

  // Public attributes
  public:
  ::cnce::Router_Frequency_Generator * freq_gen;
  adue::pio::Pin_Output pin;
};

inline Router_Switch::Router_Switch ()
: freq_gen ( 0 )
{
}

} // namespace cnce

#endif
