/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_router_output_hpp__
#define __inc_router_output_hpp__

#include <adue/pio/pin.hpp>

namespace cnce
{

// Forward declaration
class Router_Switch;
class Router_Frequency_Generator;
class Router_Stepper;

class Router_Output
{
  // Public methods
  public:
  void
  invalidate_users ();

  // Public attributes
  public:
  adue::pio::Pin_Output pin;
  ::cnce::Router_Switch * user_switch;
  ::cnce::Router_Frequency_Generator * user_freq_gen;
  ::cnce::Router_Stepper * user_stepper;
};

} // namespace cnce

#endif
