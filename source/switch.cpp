/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#include "switch.hpp"
#include "frequency_generator.hpp"

namespace cnce
{

void
Router_Switch::invalidate ()
{
  pin.set_mask ( 0 );
  freq_gen = 0;
}

void
Router_Switch::set_low ()
{
  if ( freq_gen != 0 ) {
    freq_gen->stop ();
  } else {
    pin.set_low ();
  }
}

void
Router_Switch::set_high ()
{
  if ( freq_gen != 0 ) {
    freq_gen->start ();
  } else {
    pin.set_high ();
  }
}

} // namespace cnce
