/// adue_router: Realtime numeric control software for Arduino Due.
/// \copyright See LICENSE-adue_router.txt file

#ifndef __inc_cnce_router_stepper_job_hpp__
#define __inc_cnce_router_stepper_job_hpp__

#include "stepper_job_type.hpp"
#include <inttypes.h>

namespace cnce
{

class Router_Stepper_Job
{
  // Public methods
  public:
  uint8_t
  job_type () const;

  void
  set_job_type ( uint8_t type_n );

  uint16_t
  switch_index () const;

  void
  set_switch_index ( uint16_t index_n );

  uint16_t
  stepper_sync_group_index () const;

  void
  set_stepper_sync_group_index ( uint16_t index_n );

  uint16_t
  usecs () const;

  void
  set_usecs ( uint16_t usecs_n );

  // Private attributes
  private:
  uint8_t _job_type;
  uint16_t _usecs;
};

inline uint8_t
Router_Stepper_Job::job_type () const
{
  return _job_type;
}

inline void
Router_Stepper_Job::set_job_type ( uint8_t type_n )
{
  _job_type = type_n;
}

inline uint16_t
Router_Stepper_Job::switch_index () const
{
  return _usecs;
}

inline void
Router_Stepper_Job::set_switch_index ( uint16_t index_n )
{
  _usecs = index_n;
}

inline uint16_t
Router_Stepper_Job::stepper_sync_group_index () const
{
  return _usecs;
}

inline void
Router_Stepper_Job::set_stepper_sync_group_index ( uint16_t index_n )
{
  _usecs = index_n;
}

inline uint16_t
Router_Stepper_Job::usecs () const
{
  return _usecs;
}

inline void
Router_Stepper_Job::set_usecs ( uint16_t usecs_n )
{
  _usecs = usecs_n;
}

} // namespace cnce

#endif
